new Vue({
     el: '#meApp',
     data: {
          medias: [],
          hashtag: '#web'
     },
     created: function () {
          //Gallery
          this.createGallery();
     },
     methods: {
          createGallery: function () {
               for (var i = 0; i <= 18; i++) {
                   this.medias.push({
                        "id": 'hard_rock_' +  i,
                        "link": 'img/photo_' +  i + '.jpg',
                        "img" : 'img/small/photo_' +  i + '.jpg'
                   });

               }
          }
     }
})
