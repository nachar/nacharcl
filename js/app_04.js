new Vue({
     el: '#meApp',
     data: {
          medias: [],
          hashtag: '#hnachar',
          loader: true
     },
     created: function () {
          this.getInstagramToken();
     },
     methods: {
          /*** Get ***/
          getInstagramToken: function () {
               var self = this;
               var instagramToken = '274912814.9c917c8.c83922de4f7a46d799ad03f2448885a2';
               var settings = {
                    "url": "https://api.instagram.com/v1/users/self/media/recent/",
                    "method": "GET",
                    "data": {
                         "access_token": instagramToken
                    }
               }

               $.ajax(settings)
                    .done(function (response) {
                         self.orderInstagramMedia(self, response.data);
                         self.pushLocalGalleries();
                         self.sortMediasByDate();
                         self.stopLoader();
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                         // console.log("error: getInstagramToken()");
                         self.pushLocalGalleries();
                         self.sortMediasByDate();
                         self.stopLoader();
                    });
          },
          orderInstagramMedia: function (self, data) {
               $.each(data, function(mediaIndex, media) {
                    var text = (media.caption != null) ? media.caption.text : '';
                    if (text.includes(self.hashtag)){
                         self.medias.push({
                              "id": media.id,
                              "link": media.link,
                              "img" : media.images.standard_resolution.url,
                              "dateInt": media.created_time,
                              "date": new Date(media.created_time * 1000),
                              "from": "instagram"
                         });
                    }
               });
          },
          pushLocalGalleries: function () {
               this.medias.push({
                    "id": 'sje_2017',
                    "link": 'galleries/sje_2017/index.html',
                    "img" : 'galleries/sje_2017/img/medium/photo_62.jpg',
                    "dateInt": (new Date('2017/10/08').getTime()) / 1000,
                    "date": new Date('2017/10/08'),
                    "from": "gallery"
               });

               this.medias.push({
                    "id": 'hard_rock',
                    "link": 'galleries/hardrock/index.html',
                    "img" : 'galleries/hardrock/img/medium/photo_0.jpg',
                    "dateInt": (new Date('2017/08/24').getTime()) / 1000,
                    "date": new Date('2017/08/24'),
                    "from": "gallery"
               });

               this.medias.push({
                    "id": 'cosparty_2017',
                    "link": 'galleries/cosparty2017/index.html',
                    "img" : 'galleries/cosparty2017/img/medium/photo_1.jpg',
                    "dateInt": (new Date('2017/10/04').getTime()) / 1000,
                    "date": new Date('2017/10/04'),
                    "from": "gallery"
               });

               this.medias.push({
                    "id": 'cnl_27_nov',
                    "link": 'galleries/cnl27nov/index.html',
                    "img" : 'galleries/cnl27nov/img/medium/photo_12.jpg',
                    "dateInt": (new Date('2017/11/27').getTime()) / 1000,
                    "date": new Date('2017/11/27'),
                    "from": "gallery"
               });

               this.medias.push({
                    "id": 'cnl_ago',
                    "link": 'galleries/cnl13ago/index.html',
                    "img" : 'galleries/cnl13ago/img/medium/photo_5.jpg',
                    "dateInt": (new Date('2017/08/13').getTime()) / 1000,
                    "date": new Date('2017/08/13'),
                    "from": "gallery"
               });

               this.medias.push({
                    "id": 'cami_11_2017',
                    "link": 'galleries/cami-11-2017/index.html',
                    "img" : 'galleries/cami-11-2017/img/medium/photo_5.jpg',
                    "dateInt": (new Date('2017/11/06').getTime()) / 1000,
                    "date": new Date('2017/11/06'),
                    "from": "gallery"
               });

               this.medias.push({
                    "id": 'hass_2017_01_16',
                    "link": 'galleries/hass-2018-01-16/index.html',
                    "img" : 'galleries/hass-2018-01-16/img/medium/photo_2.jpg',
                    "dateInt": (new Date('2018/01/16').getTime()) / 1000,
                    "date": new Date('2018/01/16'),
                    "from": "gallery"
               });

               this.medias.push({
                    "id": 'humboldt-2018_03_10',
                    "link": 'galleries/humboldt-2018-03-10/index.html',
                    "img" : 'galleries/humboldt-2018-03-10/img/medium/photo_12.jpg',
                    "dateInt": (new Date('2018/03/10').getTime()) / 1000,
                    "date": new Date('2018/03/10'),
                    "from": "gallery"
               });


               this.medias.push({
                    "id": 'mtalta-2018_03_10',
                    "link": 'galleries/mtalta-2018-03-10/index.html',
                    "img" : 'galleries/mtalta-2018-03-10/img/medium/photo_9.jpg',
                    "dateInt": (new Date('2018/03/10').getTime()) / 1000,
                    "date": new Date('2018/03/10'),
                    "from": "gallery"
               });

               this.medias.push({
                    "id": 'cnl-2018_03_11',
                    "link": 'galleries/cnl_2018-03-11/index.html',
                    "img" : 'galleries/cnl_2018-03-11/img/medium/photo_18.jpg',
                    "dateInt": (new Date('2018/03/11').getTime()) / 1000,
                    "date": new Date('2018/03/11'),
                    "from": "gallery"
               });
          },
          sortMediasByDate: function () {
               this.medias.sort(function(a,b){
                    return b.date - a.date;
               });
          },
          stopLoader: function () {
               this.loader = false;
          }
     }
})
